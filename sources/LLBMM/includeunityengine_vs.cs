using System;
using UnityEngine;

namespace MyFirstMod
{
	public class MyFirstMod : MonoBehaviour
	{
		public static MyFirstMod instance = null;

		public static void Initialize()
		{
			GameObject gameObject = new GameObject("MyFirstMod");
			this.instance = gameObject.AddComponent<MyFirstMod>();
			DontDestroyOnLoad(gameObject);
		}
	}
}

